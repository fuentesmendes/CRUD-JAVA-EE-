
package CRUD.MODELO;
import java.sql.*;
import com.mysql.jdbc.Connection;
import javax.swing.*;
public class clsConexion {
    
    public static final String usuario = "root";
    public static final String clave = "123456";
    public static final String host = "localhost";
    public static final String puerto = "3306";
    public static final String bdatos = "pruebas";
    public static final String clasename = "com.mysql.jdbc.Driver";
    public static final String url = "jdbc:mysql://" + host + ":" + puerto + "/" + bdatos;
    
    public java.sql.Connection con;
    
    public clsConexion()
    {
        try {
            
            Class.forName(clasename);
            con = DriverManager.getConnection(url, usuario, clave);
            JOptionPane.showMessageDialog(null, "CONEXION CORRECTA"); //Mensaje si esta conectado a la base de datos
            
        } catch (ClassNotFoundException | SQLException e) {
            
            JOptionPane.showMessageDialog(null, "CONEXION FALLIDA : \nMENSAJE : " + e); //Mensaje si no esta conectado a la base de datos
        }
    }
    
    //Probar si la conexion es correcta, pero en main public static void main(String[] args) {
    /*
    public static void main(String[] args) {
        clsConexion conectar = new clsConexion();
    }
    */
}
