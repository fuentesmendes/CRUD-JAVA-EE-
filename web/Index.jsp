<%-- 
    Document   : Index
    Created on : 10/05/2018, 02:50:28 PM
    Author     : dmendez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="CSS/bootstrap.min.css">

    <title>NUEVA PERSONA</title>
  </head>
  <body>
      <br>
      <div class="container">
          <div class="text-center"><h2><strong>NUEVA PERSONA</strong></h2></div>
      </div>
      <br>
      
      <div class="row">
          <div class="col-4"></div>
          <div class="col-4">
              <form id="frmNuevaPersona" action="svtPersona" method="POST">
                  <div class="form-group">
                      <input type="text" id="idCedula" name="txtCedula" placeholder="N° DE CEDULA" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <input type="text" id="idNombres" name="txtNombres" placeholder="NOMBRES" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <input type="text" id="idApellidos" name="txtApellidos" placeholder="APELLIDOS" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <input type="number" id="idEdad" name="txtEdad" placeholder="EDAD" class="form-control"/>
                  </div>
                  <div class="form-group">
                      <input type="submit" id="idGuardar" name="btnGuardar" value="AGREGAR" class="btn btn-primary btn-block"/>
                      <input type="button" id="idCancelar" name="btnCancelar" value="CANCELAR" class="btn btn-danger btn-block"/>
                  </div>
              </form>
          </div>
          <div class="col-4"></div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="JS/jquery.slim.min.js"></script>
    <script src="JS/jquery.js"></script>
    <script src="JS/popper.min.js"></script>
    <script src="JS/bootstrap.min.js"></script>
  </body>
</html>
